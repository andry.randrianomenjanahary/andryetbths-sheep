from random import*
class Chasseur:
    def __init__(self,position_x,position_y):
        self.position_x = position_x
        self.position_y = position_y
    
    def seDeplacer_chasseur(self,tableau):
        # Déplacer le chasseur aléatoirement dans une des 8 cases adjacentes (en considérant le monde comme torique)
        x = randint(-1,1)
        y = randint(-1,1)
        self.position_x = (self.position_x + x)%tableau.dimension
        self.position_y = (self.position_y + y)%tableau.dimension
        return self.position_x,self.position_y
    
    def placeChasseur(self, px, py):
        # Placer le chasseur aux coordonnées (i, j)
        self.position_x = px
        self.position_y = py
