from random import randint
class Monde :
    def __init__(self, dimension, duree_repousse):
        self.dimension = dimension
        self.duree_repousse = duree_repousse
        self.carte = [[randint(0,duree_repousse*2) for i in range(dimension)] for j in range(dimension)]
        
    def herbePousse(self):
        # Faire repousser l'herbe dans les cases de la matrice monde
        for i in range (len(self.carte)):
            for j in range(len(self.carte)):
                if self.carte[i][j] < self.duree_repousse:
                    self.carte[i][j] += 1
        return self.carte
    
    def is_empty(self,i,j):
        return self.carte[i][j] < self.duree_repousse
            
    def herbeMangee(self,i,j):
        # Modifier la valeur de la case (i, j) de la matrice carte pour indiquer qu'il n'y a plus d'herbe
        if not self.is_empty(i,j):
            self.carte[i][j] = 0
        
    def nbHerbe(self):
        # Retourner le nombre de cases qui contiennent de l'herbe dans la matrice carte
        cpt = 0
        for i in range (len(self.carte)):
            for j in range(len(self.carte)):
                if self.carte[i][j] > self.duree_repousse:
                    cpt += 1
        return cpt
    
    def getCoefCarte(self,i,j):
        # Retourner la valeur du coefficient (i, j) de la matrice carte
        return self.carte[i][j]


    
