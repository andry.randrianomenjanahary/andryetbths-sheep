from random import*
from classe_monde import*
from classe_mouton import*
from classe_loup import*
from classe_chasseur import*
import matplotlib.pyplot as plt
import numpy as nb

class Simulation:
    def __init__(self, nombre_moutons, nombre_loups, nombre_chasseurs, fin_du_monde, monde, max_moutons):
        self.nombre_moutons = nombre_moutons
        self.nombre_loups = nombre_loups
        self.nombre_chasseurs = nombre_chasseurs
        self.horloge = 0
        self.fin_du_monde = fin_du_monde
        self.monde = monde
        self.moutons = [Mouton(randint(0,monde.dimension-1), randint(0,monde.dimension-1)) for i in range(nombre_moutons)]
        self.loups = [Loup(randint(0,monde.dimension-1), randint(0,monde.dimension-1)) for i in range(nombre_loups)]
        self.chasseurs = [Chasseur(randint(0,monde.dimension-1), randint(0,monde.dimension-1)) for i in range(nombre_chasseurs)]
        self.max_moutons = max_moutons
        self.resultats_herbe = []
        self.resultats_moutons = []
        self.resultats_loups = []
        self.resultats_chasseurs = []
        
    def clock(self):
        self.horloge += 1
    
    def herbPousse(self):
        self.monde.herbePousse()
        
    def deplace_mouton(self):
        for i in self.moutons:
            i.seDeplacer(self.monde)
            
    def deplace_loup(self):
        for j in self.loups:
            j.seDeplacer_loup(self.monde)
        
    def deplace_chasseur(self):
        for c in self.chasseurs:
            c.seDeplacer_chasseur(self.monde)
        
    def vitale_mouton(self):
        for i in self.moutons:
            i.variationEnergie(self.monde)
            if i.energie == 0:
                self.moutons.remove(i)
                
    def vitale_loup(self):
        for i in self.loups:
            if i.energie == 0:
                self.loups.remove(i)
                
        
    def taux_reproduction(self):
        m = len(self.moutons)
        for i in range(m):
            x = randint(0,100)
            if x <= 4:
                self.moutons.append(Mouton(self.moutons[i].position_x, self.moutons[i].position_y))
    def taux_reproduction_loup(self):
        l = len(self.loups)
        for j in range(l):
            x = randint(0,100)
            if x <= 5:
                self.loups.append(Loup(self.loups[j].position_x, self.loups[j].position_y))
    
    def mouton_manger(self):
        for j in self.loups:
            for i in self.moutons:
                # Vérifier s'il y a un mouton sur la case (i, j)
                if i.position_x == j.position_x and i.position_y == j.position_y:
                    # Supprimer le mouton de la liste des moutons du monde
                    self.moutons.remove(i)
                    j.energie += j.gain_nourriture
                else:
                    j.energie -= 1
    
    def loup_tuer(self):
        for c in self.chasseurs:
            for j in self.loups:
                if c.position_x == j.position_x and c.position_y == j.position_y:
                    self.loups.remove(j)
                
            
            
    def resultats(self):
        self.resultats_herbe.append(self.monde.nbHerbe())
        self.resultats_moutons.append(len(self.moutons))
        self.resultats_loups.append(len(self.loups))
        self.resultats_chasseurs.append(len(self.chasseurs))
        
    def simMouton(self):
        while self.horloge < self.fin_du_monde and self.nombre_moutons < self.max_moutons:
            self.clock()
            self.herbPousse()
            self.deplace_mouton()
            self.deplace_loup()
            self.deplace_chasseur()
            self.vitale_mouton()
            self.vitale_loup()
            self.mouton_manger()
            self.loup_tuer()
            self.taux_reproduction()
            self.taux_reproduction_loup()
            self.resultats()
            
    def fin(self):
        return self.resultats_herbe, self.resultats_moutons, self.resultats_loups, self.resultats_chasseurs
    def graphe(self):
        x = [i for i in range(self.fin_du_monde)]
        y1 = nb.array(self.resultats_herbe)
        y2 = nb.array(self.resultats_moutons)
        y3 = nb.array(self.resultats_loups)
        plt.plot(y1,label='herbes')
        plt.plot(y2,label='moutons')
        plt.plot(y3,label='loups')
        plt.legend()
        plt.show()
        

