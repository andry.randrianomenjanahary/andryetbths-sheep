from random import*
from classe_monde import*
class Mouton:
    def __init__(self,position_x, position_y,):
        self.position_x = position_x
        self.position_y = position_y
        self.gain_nourriture = 4
        self.taux_reproduction = 4
        self.energie = randint(1,2*self.gain_nourriture)
    
    def variationEnergie(self,tableau):
        if tableau.getCoefCarte(self.position_x,self.position_y) > 0:
            tableau.herbeMangee(self.position_x,self.position_y)
            self.energie += self.gain_nourriture      
        else:
            self.energie -= 1
        return self.energie
    
    def seDeplacer(self,tableau):
        # Déplacer le mouton aléatoirement dans une des 8 cases adjacentes (en considérant le monde comme torique)
        x = randint(-1,1)
        y = randint(-1,1)
        self.position_x = (self.position_x + x)%tableau.dimension
        self.position_y = (self.position_y + y)%tableau.dimension
        return self.position_x,self.position_y
    def placeMouton(self,px,py):
        # Placer le mouton aux coordonnées (i, j)
        self.position_x = px
        self.position_y = py
        