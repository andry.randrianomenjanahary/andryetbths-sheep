from random import*
from classe_monde import*
from classe_mouton import*

class Loup:
    def __init__(self,position_x,position_y):
        self.position_x = position_x
        self.position_y = position_y
        self.gain_nourriture = 18
        self.taux_reproduction = 5
        self.energie = randint(1,2*self.gain_nourriture)
    
    def seDeplacer_loup(self,tableau):
        # Déplacer le loup aléatoirement dans une des 8 cases adjacentes (en considérant le monde comme torique)
        x = randint(-1,1)
        y = randint(-1,1)
        self.position_x = (self.position_x + x)%tableau.dimension
        self.position_y = (self.position_y + y)%tableau.dimension
        return self.position_x,self.position_y
    def placeLoup(self,px,py):
        # Placer le loup aux coordonnées (i, j)
        self.position_x = px
        self.position_y = py